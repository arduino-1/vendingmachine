﻿namespace MoneyController
{
	public class MoneyReceivedEventHandlerArgs
	{
		public decimal Value { get; set; }
	}
}